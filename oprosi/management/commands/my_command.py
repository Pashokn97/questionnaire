import requests
from django.core.management.base import BaseCommand

from oprosi.models import Question, QuestionType


class GetQuestion:

    def add_questions(self, amount):
        lst = []
        for iteration in range(amount // 100):
            lst.append(self.get_questions(iteration * 100))
        return lst

    def get_questions(self, offset=0):
        resp = requests.get(f'http://jservice.io/api/clues?value=100&offset={offset}')
        return [question['question'] for question in resp.json()]


class QuestionAdaptation:

    def _validate(self, amount):
        if amount % 100 != 0:
            raise Exception('Число должно быть кратно 100')

    def prepare_to_save(self, amount):
        lst = []
        for question in GetQuestion().add_questions(amount):
            lst.append(Question(
                text_question=question,
                type_question=QuestionType.text_answer,
                interview_id=1
            ))
        return lst

    def handle(self, amount):
        self._validate(amount)
        Question.objects.bulk_create(self.prepare_to_save(amount))


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('amount', type=int, help='Указывает количество вопросов')

    def handle(self, *args, **options):
        amount = options['amount']
        QuestionAdaptation().handle(amount)

