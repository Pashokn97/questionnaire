from rest_framework import permissions
from rest_framework.permissions import BasePermission
from rest_framework.viewsets import ModelViewSet


class AdminPermission(BasePermission):
    def has_permission(self, request, view):
        return {
            'update': bool(request.user),
            'create': bool(request.user and request.user.is_staff),
            'list': bool(request.user)
        }.get(view.action)