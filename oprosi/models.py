from django.contrib.auth.models import AbstractUser
from django.db import models


class Status(models.TextChoices):
    new = 'Готова к прохождения'
    in_process = 'В процессе'
    finished = 'Завершен'
    pause = 'На паузе'


class QuestionType(models.TextChoices):
    text_answer = 'Ответ текстом'
    choose_one = 'Выберите один вариант'


class CustomUser(AbstractUser):
    pass


class Interview(models.Model):
    name = models.CharField(max_length=100, )
    description = models.TextField(max_length=500)

    class Meta:
        verbose_name = 'Опрос'


class Question(models.Model):
    text_question = models.TextField(max_length=500, verbose_name='текст вопроса')
    type_question = models.CharField(choices=QuestionType.choices, max_length=100, verbose_name='тип вопроса')
    interview = models.ForeignKey('interview', on_delete=models.CASCADE, verbose_name='опрос')

    class Meta:
        verbose_name = 'Вопросы'


class Answer(models.Model):
    interview = models.ForeignKey('Interview', on_delete=models.CASCADE, verbose_name='ID опроса')
    answer = models.CharField(max_length=300, blank=True, verbose_name='ответ')
    interviewer = models.ForeignKey('CustomUser', on_delete=models.CASCADE, verbose_name='ID опрашиваемого')
    question = models.ForeignKey('Question', on_delete=models.CASCADE, verbose_name='ID вопроса')
    session = models.ForeignKey('Session', on_delete=models.CASCADE, verbose_name='ID сессии')

    class Meta:
        verbose_name = 'Ответы'


class Session(models.Model):
    started_at = models.DateTimeField(verbose_name='начало сессии', blank=True, null=True)
    ended_at = models.DateTimeField(verbose_name='конец сессии', blank=True, null=True)
    status = models.CharField(choices=Status.choices, max_length=100, verbose_name='статус сессии', default=Status.new)
    interviewer = models.ForeignKey('CustomUser', on_delete=models.CASCADE, verbose_name='ID опрашиваемого')
    interview = models.ForeignKey('Interview', on_delete=models.CASCADE,  verbose_name='ID опроса')

    class Meta:
        verbose_name = 'Сессии'
