from datetime import datetime
from rest_framework.exceptions import ValidationError
from .models import CustomUser, Answer, Interview, Session, Question, Status
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    repeat_password = serializers.CharField(max_length=100, write_only=True)

    class Meta:
        model = CustomUser
        fields = ['username', 'password', 'repeat_password', 'is_staff']

    def create(self, validated_data):
        password = validated_data.pop('password')
        validated_data.pop('repeat_password')
        user = CustomUser.objects.create(**validated_data)
        user.set_password(password)
        user.save()
        return user

    def validate(self, attrs):
        if attrs['password'] != attrs['repeat_password']:
            raise ValidationError('Пароли не совпадают')
        return attrs


class QuestionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = ['text_question', 'type_question', 'interview']


class ManyQuestionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = ['interview']


class InterviewSerializer(serializers.ModelSerializer):

    class Meta:
        model = Interview
        fields = ['name', 'description']


class AnswerSerializer(serializers.ModelSerializer):
    interviewer = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = Answer
        fields = ['answer', 'interview',  'interviewer', 'question', 'session']

    def validate(self, attrs):
        session = Session.objects.get(id=attrs['session'].id)
        if attrs['session'].status != Status.in_process:
            raise ValidationError('Нельзя отвечать на вопрос, если сессия не активна')
        elif attrs['interviewer'] != session.interviewer:
            raise ValidationError('Не этот пользователь проходит опрос')
        return attrs


class SessionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Session
        fields = ['interviewer', 'interview']


class SessionUpdateSerializer(serializers.ModelSerializer):

    status = serializers.ChoiceField(choices=[
        Status.in_process,
        Status.finished,
        Status.pause
    ])

    class Meta:
        model = Session
        fields = ['status', 'interviewer', 'interview']

    def validate(self, attrs):
        if attrs['status'] == Status.in_process:
            if self.instance.status == Status.new:
                self.instance.started_at = datetime.now()
            elif self.instance.status == Status.pause:
                return attrs
            else:
                raise ValidationError('Сессия все еще не начата или завершена')
        elif attrs['status'] == Status.pause:
            if self.instance.status != Status.in_process:
                raise ValidationError('Сессия не начата или завершена')
        elif attrs['status'] in Status.finished:
            if self.instance.status == Status.new:
                raise ValidationError('Сессия еще не начата')
            else:
                self.instance.ended_at = datetime.now()
        return attrs