from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from .models import CustomUser, Question, Interview, Answer, Session
from .permissions import AdminPermission
from .serializers import UserSerializer, QuestionsSerializer, InterviewSerializer, AnswerSerializer, SessionSerializer, \
    SessionUpdateSerializer, ManyQuestionsSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from .service import get_questions
from .tasks import handle


class InterviewViewSet(ModelViewSet):
    queryset = Interview.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = InterviewSerializer


class CreateUser(generics.CreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny, )


class QuestionsViewSet(ModelViewSet):
    queryset = Question.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = QuestionsSerializer


class QuestionsCreate(generics.CreateAPIView):
    serializer_class = ManyQuestionsSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if questions := get_questions():
            questions_for_create = [
                Question(
                    text_question=question,
                    type_question='Ответ текстом',
                    interview=serializer.validated_data['interview']
                ) for question in questions]
            Question.objects.bulk_create(questions_for_create)
            return Response()
        return Response(status=400)


class ListInterview(generics.ListAPIView):
    queryset = Interview.objects.all()
    serializer_class = InterviewSerializer
    permission_classes = (AllowAny,)


class InterviewerAnswer(generics.ListCreateAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.queryset.all()
        return self.queryset.filter(interviewer=self.request.user)


class InterviewSession(mixins.CreateModelMixin,
                       mixins.ListModelMixin,
                       mixins.UpdateModelMixin,
                       GenericViewSet):
    queryset = Session.objects.all()
    serializer_class = SessionSerializer
    permission_classes = (AdminPermission, )

    def get_serializer_class(self):
        serializer = {
            'create': SessionSerializer,
            'update': SessionUpdateSerializer,
            'list': SessionSerializer
        }
        return serializer[self.action]

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.queryset.all()
        return self.queryset.filter(interviewer=self.request.user)


class LoadQuestionView(APIView):

    def get(self, request):
        handle.delay(100)
        return Response()

