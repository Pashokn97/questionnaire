import logging

from oprosi.management.commands.my_command import QuestionAdaptation
from oprosnik.celery import app

logger = logging.getLogger(__name__)


@app.task
def handle(amount):
    logger.info('start')
    QuestionAdaptation().handle(amount)
    logger.info('stop')
