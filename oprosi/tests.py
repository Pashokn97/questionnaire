# from collections import OrderedDict
import datetime
from collections import OrderedDict
from django.urls import reverse
from rest_framework import status
from oprosi.models import Interview, Answer, CustomUser, Question, Session
from unittest.mock import patch
from rest_framework.test import APITestCase
from oprosi.serializers import InterviewSerializer, AnswerSerializer


class GetMethodTest(APITestCase):
    def test_get(self):
        interview = Interview.objects.create(name='Интервью', description='Инетерестное интервью')
        interview2 = Interview.objects.create(name='Интервью2', description='Инетерестное интервью')
        url = reverse('interview')
        response = self.client.get(url)
        serializer_data = InterviewSerializer([interview, interview2], many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)

    def test_get_answers(self):
        interview = Interview.objects.create(name='abc', description='abc')
        user1 = CustomUser.objects.create(username='Pasha', password='Parish022', is_staff=False)
        user2 = CustomUser.objects.create(username='Pasha1', password='Parish022', is_staff=True)
        question = Question.objects.create(text_question='text', type_question='Ответ текстом', interview=interview)
        session = Session.objects.create(started_at=datetime.datetime.now(), ended_at=None, status='В процессе',
                                         interviewer=user1, interview=interview)
        session2 = Session.objects.create(started_at=datetime.datetime.now(), ended_at=None, status='В процессе',
                                         interviewer=user2, interview=interview)
        answer1 = Answer.objects.create(interview=interview, answer='answer', interviewer=user1, question=question,
                                        session_id=session.id)
        answer2 = Answer.objects.create(interview=interview, answer='answer', interviewer=user2, question=question,
                                        session_id=session2.id)
        url = reverse('answers')
        if self.client.force_authenticate(user1):
            response = self.client.get(url)
            serializer_data = AnswerSerializer(answer1).data
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual([OrderedDict(serializer_data)], response.data)
        if self.client.force_authenticate(user2):
            response = self.client.get(url)
            serializer_data = AnswerSerializer([answer1, answer2], many=True).data
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual([OrderedDict(serializer_data)], response.data)

    def test_session_post(self):
        interview = Interview.objects.create(name='abc', description='abc')
        user1 = CustomUser.objects.create(username='Pasha', password='Parish022', is_staff=True)
        self.client.force_authenticate(user1)
        url = '/api/session/'
        data = {'status': 'Готова к прохождению', 'interviewer': user1.pk, 'interview': interview.pk}
        response = self.client.post(url, data)

    def test_custom_user(self):
        url = reverse('custom-user')
        data = {'username': 'Pasha', 'password': 'Parish022', 'repeat_password': 'Parish022', 'is_staff': True}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class QuestionsTest(APITestCase):
    @patch('oprosi.views.get_questions')
    def test_get_questions(self, mock):
        user1 = CustomUser.objects.create(username='Pasha', password='Parish022', is_staff=True)
        interview = Interview.objects.create(name='abc', description='abc')
        self.client.force_authenticate(user1)
        url = reverse('many-questions')
        mock.return_value = ['Вопрос1', 'Вопрос2']
        response = self.client.post(url, {'interview': 1})
        questions = Question.objects.values_list('text_question', flat=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual([question for question in questions], mock.return_value)



