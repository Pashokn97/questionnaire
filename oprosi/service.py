import requests
from rest_framework.status import is_success


def get_questions(min_date='1985-02-20'):
    resp = requests.get(f'http://jservice.io/api/clues?value=100&min_date={min_date}')
    if is_success(resp.status_code):
        return [quest['question'] for quest in resp.json()]

